package geoquiz.codingcamp.com.codingcamp_geoquiz;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;


public class Aufgabenhelfer {

    private static final String TAG = Aufgabenhelfer.class.getSimpleName();
    private Context context;
    private Aufgabe[] aufgaben;

    public Aufgabenhelfer(@NonNull Context context) {
        this.context = context;
        ladeAufgaben(context.getAssets());
    }

    @NonNull
    public Aufgabe holeAufgabe() {
        int zufallszahl = new Random().nextInt(aufgaben.length);

        return aufgaben[zufallszahl];
    }


    private void ladeAufgaben(@NonNull AssetManager assetManager) {
        Gson gson = new Gson();

        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            assetManager.open("aufgaben.json"),
                            "utf-8"));

            aufgaben = gson.fromJson(reader, Aufgabe[].class);

        } catch (Exception e) {
            Log.e(TAG, "ladeAufgaben: ", e);
            System.exit(-1);
        }
    }
}
