package geoquiz.codingcamp.com.codingcamp_geoquiz;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;


class Aufgabe {

    private static final String TAG = Aufgabe.class.getSimpleName();
    private String zielname;
    private String frage;
    private double latziel;
    private double longziel;

    public LatLng zielkoordinate() {
        LatLng latLng = new LatLng(latziel, longziel);

        Log.d(TAG, "zielkoordinate: " + latLng);

        return latLng;
    }

    public String zielname() {
        return zielname;
    }

    public String frage() {
        if (frage.equals("")) {
            return zielname();
        }
        return frage;
    }

}
