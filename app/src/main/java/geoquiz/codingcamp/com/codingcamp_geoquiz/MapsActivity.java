package geoquiz.codingcamp.com.codingcamp_geoquiz;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    Button buttonNeueFrage, buttonAntwortAbgeben;
    TextView textViewGesuchterOrt, textViewAbstandZiel;
    ImageView imageViewFadenkreuz;
    Aufgabenhelfer aufgabenhelfer = null;
    Aufgabe aufgabe;
    private GoogleMap landkarte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        aufgabenhelfer = new Aufgabenhelfer(this);

        verbindeBedienelementMitProgramm();
        setzeLaucherAufBedienelemente();


        spielfeldEinstellen(false, View.INVISIBLE, 0);
    }

    private void verbindeBedienelementMitProgramm() {
        textViewGesuchterOrt = findViewById(R.id.tv_city);
        textViewAbstandZiel = findViewById(R.id.tv_Points);

        buttonNeueFrage = findViewById(R.id.bt_new);
        buttonAntwortAbgeben = findViewById(R.id.bt_accept);

        imageViewFadenkreuz = findViewById(R.id.imageMarker);
    }

    private void setzeLaucherAufBedienelemente() {
        buttonNeueFrage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                neueFrageStellen();
            }
        });
        buttonAntwortAbgeben.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                antwortAbgeben();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        landkarte = googleMap;

        karteZurueckstetzen();
    }

    public void karteZurueckstetzen() {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(50, 8))
                .zoom(4)
                .tilt(0)
                .build();
        landkarte.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        landkarte.setMapType(MAP_TYPE_NORMAL);
        landkarte.setMaxZoomPreference(10);
        landkarte.clear();
        landkarte.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                this, R.raw.style_json));
    }

    public void spielfeldEinstellen(boolean antwortButtonSichtbar,
                                    int sichtbarkeitFadenkreuz,
                                    int abstandZiel) {

        buttonAntwortAbgeben.setEnabled(antwortButtonSichtbar);
        imageViewFadenkreuz.setVisibility(sichtbarkeitFadenkreuz);
        textViewAbstandZiel.setText(getString(R.string.distance, abstandZiel));
    }

    public void neueFrageStellen() {
        karteZurueckstetzen();
        aufgabe = aufgabenhelfer.holeAufgabe();
        textViewGesuchterOrt.setText(aufgabe.frage());

        spielfeldEinstellen(true, View.VISIBLE, 0);
    }


    public void antwortAbgeben() {
        LatLng benutzerAntwortPosition = landkarte.getCameraPosition().target;
        LatLng korrekteAntwortPosition = aufgabe.zielkoordinate();

        zoomZuErgebnis(benutzerAntwortPosition, korrekteAntwortPosition);

        int abstand = berechneAbstand(benutzerAntwortPosition, korrekteAntwortPosition);

        spielfeldEinstellen(false, View.INVISIBLE, abstand);
    }


    public Marker holeBenutzerAntwortMarker(LatLng latlng) {
        return landkarte.addMarker(new MarkerOptions()
                .position(latlng)
                .title("Ausgewählt")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
    }


    public Marker holeKorrekteAntwortMarker(LatLng latlng) {
        return landkarte.addMarker(new MarkerOptions()
                .position(latlng)
                .title(aufgabe.zielname())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
    }


    public void zoomZuErgebnis(LatLng benutzerAntwortPosition, LatLng korrekteAntwortPosition) {

        Marker benutzerAntwortMarker = holeBenutzerAntwortMarker(benutzerAntwortPosition);
        Marker korrekteAntwortMarker = holeKorrekteAntwortMarker(korrekteAntwortPosition);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(benutzerAntwortMarker.getPosition());
        builder.include(korrekteAntwortMarker.getPosition());

        LatLngBounds bounds = builder.build();
        int padding = 150; // in pixels

        landkarte.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                this, R.raw.style_json_labels));
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        landkarte.animateCamera(cu);
    }


    public int berechneAbstand(LatLng benutzerAntwortPosition, LatLng korrekteAntwortPosition) {

        Location location_user = new Location("Location_User");
        location_user.setLatitude(benutzerAntwortPosition.latitude);
        location_user.setLongitude(benutzerAntwortPosition.longitude);

        Location location_target = new Location("Location_User");
        location_target.setLatitude(korrekteAntwortPosition.latitude);
        location_target.setLongitude(korrekteAntwortPosition.longitude);

        double distance = location_user.distanceTo(location_target);

        return (int) Math.round(distance / 1000);
    }
}
